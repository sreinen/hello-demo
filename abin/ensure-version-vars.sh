#! /bin/bash

# dot (source) this file in to scripts that need to have a default app_version set when running locally

if [ -f ${APP_VERSION} ]; then
  echo "WARNING: APP_VERSION environment variable not set. Defaulting to 'latest'. Robots should set APP_VERSION to a valid SemVer string."
  APP_VERSION=latest
fi

# Hard coding chart version for now because version numbers are hard.
# Want to set it up so that if the app_version is a valid semver string (not just not 'latest'), we set chart_version to app_version
CHART_VERSION=0.1.0
